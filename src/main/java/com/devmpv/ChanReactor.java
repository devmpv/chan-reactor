package com.devmpv;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChanReactor {

	public static void main(String[] args) {
		SpringApplication.run(ChanReactor.class, args);
	}
}
