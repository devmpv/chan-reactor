package com.devmpv.config;

public interface Const {

	public interface Thumbs {
		int WIDTH = 200;
		int HEIGHT = 200;
	}

	String TITLE = "title";
	String TEXT = "text";
	String BOARD = "board";
	String THREAD = "thread";
}
